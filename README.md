# ProfileBot
[![Discord](https://img.shields.io/discord/564837852718956580.svg?logo=discord&label=)](https://discord.gg/N2xed6s)
[![Pipeline](https://gitlab.com/servertoolsbot/profilebot/badges/master/pipeline.svg)](https://gitlab.com/servertoolsbot/profilebot/commits/master)

Stand-alone bot for the profile function of ServerTools

<!--Trigger dependency update-->
