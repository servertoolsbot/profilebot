package io.github.servertoolsbot.profilebot.commands;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

public class Command {
    public static MessageCreateEvent event;
    public static String command;
    public static TextChannel channel;
    public static User user;
    public static MessageAuthor author;
    public static String[] param;
    public static String[] content;

    public static void copyToClass(MessageCreateEvent event, String command, TextChannel channel, User user, MessageAuthor author, String[] param, String[] content) {
        Command.event = event;
        Command.command = command;
        Command.channel = channel;
        Command.user = user;
        Command.author = author;
        Command.param = param;
        Command.content = content;
    }
}
