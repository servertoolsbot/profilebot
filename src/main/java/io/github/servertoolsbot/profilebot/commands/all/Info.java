package io.github.servertoolsbot.profilebot.commands.all;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.GetEmbed;

public class Info {
    public static void execute() {
        Command.channel.sendMessage(GetEmbed.gray().setTitle("Information").setDescription("I'm a bot created for the purpose of linking to profiles Discord has no integration for\n\nYou can visit my developers at my support server: https://discord.gg/N2xed6s\nOr my repo: https://gitlab.com/ServerToolsBot/ProfileBot"));
    }
}
