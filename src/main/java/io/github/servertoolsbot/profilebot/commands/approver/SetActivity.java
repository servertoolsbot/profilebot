package io.github.servertoolsbot.profilebot.commands.approver;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.Flag;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.channel.TextChannel;

public class SetActivity {
    public static void execute() {
        DiscordApi api = ApiInstance.get();
        TextChannel channel = Command.channel;
        String[] param = Command.param;
        String[] content = Command.content;

        if (param.length > 1) {
            if (param[1].equalsIgnoreCase("none")) {
                api.unsetActivity();
                channel.sendMessage(GetEmbed.green().setTitle("Unset").setDescription("Activity has been unset"));
            } else {
                StringBuilder activity = new StringBuilder();
                for (int i = 1; i < param.length; i++) {
                    if (i != 1) {
                        activity.append(" " + param[i]);
                    } else {
                        activity.append(param[i]);
                    }
                }

                String oldactivity = "none";
                if (api.getActivity().isPresent()) {
                    oldactivity = "`" + api.getActivity().get().getType() + " " + api.getActivity().get().getName() + "`";
                }

                ActivityType type = ActivityType.PLAYING;
                String newType = type.name();
                if (Flag.hasFlag("type", content)) {
                    newType = Flag.getFlag("type", content);

                    if ((newType.equalsIgnoreCase("watching") || newType.equalsIgnoreCase("watch")) || (newType.equalsIgnoreCase("listening") || newType.equalsIgnoreCase("listen")) || (newType.equalsIgnoreCase("streaming") || newType.equalsIgnoreCase("stream"))) {
                        if (newType.equalsIgnoreCase("watching") || newType.equalsIgnoreCase("watch")) {
                            type = ActivityType.WATCHING;
                        } else if (newType.equalsIgnoreCase("listening") || newType.equalsIgnoreCase("listen")) {
                            type = ActivityType.LISTENING;
                        } else if (newType.equalsIgnoreCase("stream")) {
                            type = ActivityType.STREAMING;
                        }
                        /*
                         * TODO:
                         * Add streaming
                         */
                    }
                }

                channel.sendMessage(GetEmbed.green().setTitle("Activity set").setDescription("Acitivty has been updated from ${0} to `${1}`".replace(Get.Var(0), oldactivity).replace(Get.Var(1), type.toString() + " " + activity.toString().replace(Flag.toFlag("type") + " " + newType, "").replace(Flag.toFlag("url") + " " + Flag.getFlag("url", content), ""))));

                api.updateActivity(type, activity.toString().replace(Flag.toFlag("type") + " " + newType, ""));
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Missing a new activity"));
        }
    }
}
