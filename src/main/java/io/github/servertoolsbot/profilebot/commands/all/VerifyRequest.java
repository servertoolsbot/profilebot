package io.github.servertoolsbot.profilebot.commands.all;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.CreateFiles;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.logging.Log;
import io.github.servertoolsbot.profilebot.util.profiles.Profile;
import io.github.servertoolsbot.profilebot.util.profiles.Profilisize;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.user.User;

import java.io.File;

public class VerifyRequest {
    public static void execute() {
        String[] param = Command.param;
        User user = Command.user;
        TextChannel channel = Command.channel;
        DiscordApi api = ApiInstance.get();

        if (param.length > 2) {
            if (Profile.isProfileType(param[2])) {
                if (Profile.exists(user, param[2])) {
                    if (!Profile.isVerified(user, param[2])) {
                        File requestfile = new File(Get.dir + "/cache/requests.json");
                        if (!requestfile.exists()) {
                            CreateFiles.requests();
                        }

                        JsonObject obj = GetObject.get(requestfile);

                        Integer latestid = obj.get("latestid").getAsInt();
                        latestid += 1;


                        obj.remove("latestid");
                        obj.addProperty("latestid", String.valueOf(latestid));

                        obj.addProperty(String.valueOf(latestid), user.getIdAsString() + "_" + param[2]);

                        Storeback.toFile(requestfile, obj);

                        channel.sendMessage(GetEmbed.green().setTitle("Request sent").setDescription("Your request for verifing your ${0} profil has been sent, your ID is `${1}`".replace(Get.Var(0), Profilisize.execute(param, 2)).replace(Get.Var(1), String.valueOf(latestid))));
                        if (api.getTextChannelById(GetKey.settings("verifyrequestchannel")).isPresent()) {
                            api.getTextChannelById(GetKey.settings("verifyrequestchannel")).ifPresent(channel1 -> channel1.sendMessage(GetEmbed.gray().setTitle("Verify request for " + Profilisize.execute(param, 2)).setDescription("**Link:** " + Profile.getLink(user.getIdAsString(), param[2]) + "\n**ID:** " + GetKey.get(requestfile, "latestid").getAsString()).setAuthor(user)));
                        } else {
                            Log.fatal("Request channel couldn't be found");
                        }
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Already verified").setDescription("This profile is already verified"));
                    }
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Not linked").setDescription("You aren't linked with a profile of this type"));
                }
            } else {
                channel.sendMessage(GetEmbed.red().setTitle("Not allowed").setDescription("This profile is not allowed"));
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Missing profie").setDescription("Please provide us with the name of a profile"));
        }
    }
}
