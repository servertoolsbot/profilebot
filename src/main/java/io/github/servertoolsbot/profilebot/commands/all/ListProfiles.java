package io.github.servertoolsbot.profilebot.commands.all;

import com.vdurmont.emoji.EmojiParser;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.commands.CommandArrays;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.logging.Exception;
import io.github.servertoolsbot.profilebot.util.profiles.Profile;
import io.github.servertoolsbot.profilebot.util.profiles.ProfileFile;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.user.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ListProfiles {
    public static void execute() {
        TextChannel channel = Command.channel;
        User user = Command.user;
        String[] param = Command.param;

        String id = user.getIdAsString();

        if (param.length > 1) {
            param[1] = param[1]
                    .replace("<", "")
                    .replace(">", "")
                    .replace("@", "")
                    .replace("!", "");
            if (param[1].length() == 18) {
                id = param[1];
            }
        }

        File pfile = new File(Get.dir + "/cache/users/" + id + ".json");

        StringBuilder sb = new StringBuilder();
        if (ProfileFile.exists(id)) {
            try (BufferedReader br = new BufferedReader(new FileReader(pfile))) {

                String st;
                StringBuilder text = new StringBuilder();
                while ((st = br.readLine()) != null)
                    text.append(st);

                if (!text.toString().equalsIgnoreCase("{}")) {
                    String[] profiles = CommandArrays.profiles;
                    String[] display = CommandArrays.profiledis;
                    String[] links = CommandArrays.profilelinks;

                    for (int i = 0; i < profiles.length; i++) {
                        if (GetKey.get(pfile, profiles[i]) != null) {
                            sb.append("\n**${2}:** [${0}](${1}${0})".replace(Get.Var(0), GetKey.get(pfile, profiles[i]).getAsString()).replace(Get.Var(1), links[i]).replace(Get.Var(2), display[i]));
                            if (Profile.isVerified(id, profiles[i])) {
                                sb.append(" " + EmojiParser.parseToUnicode(":white_check_mark:"));
                            }
                        }
                    }

                    DiscordApi api = ApiInstance.get();
                    if (api.getUserById(id).isDone()) {
                        try {
                            user = api.getUserById(id).get();
                        } catch (InterruptedException | ExecutionException e) {
                            Exception.print(e);
                        }
                    }

                    channel.sendMessage(GetEmbed.green().setTitle("Profiles of " + user.getName()).setDescription(sb.toString()).setAuthor(user));
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Not linked").setDescription("This user isn't linked with any profiles")); // Same messages is used right under
                }
            } catch (IOException e) {
                Exception.print(e);
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Not linked").setDescription("This user isn't linked with any profiles")); // Same message is used right above
        }
    }
}
