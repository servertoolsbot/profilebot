package io.github.servertoolsbot.profilebot.commands.approver;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import org.javacord.api.entity.channel.TextChannel;

public class Say {
    public static void execute() {
        TextChannel channel = Command.channel;
        String[] param = Command.param;

        if (param.length > 1) {
            StringBuilder message = new StringBuilder();
            for (int i = 1; i < param.length; i++) {
                if (i != 1) {
                    message.append(" " + param[i]);
                } else {
                    message.append(param[i]);
                }
            }

            channel.sendMessage(message.toString());
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Missing message").setDescription("Please provide a message"));
        }
    }
}
