package io.github.servertoolsbot.profilebot.commands.approver;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.CreateFiles;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.profiles.Profile;
import io.github.servertoolsbot.profilebot.util.profiles.ProfileFile;
import org.javacord.api.entity.channel.TextChannel;

import java.io.File;

public class Remove {
    public static void execute() {
        String[] param = Command.param;
        TextChannel channel = Command.channel;

        if (param.length > 1) {
            if (param[1].equalsIgnoreCase("verify")) {
                if (param.length > 2) {
                    if (param.length > 3) {
                        param[2] = param[2]
                                .replace("<", "")
                                .replace("@", "")
                                .replace("!", "")
                                .replace(">", "");
                        File pfile = new File(Get.dir + "/cache/users/" + param[2] + ".json");
                        if (ProfileFile.exists(param[2])) {
                            JsonObject obj = GetObject.get(pfile);
                            if (Profile.exists(param[2], param[3])) {
                                if (Profile.isVerified(param[2], param[3])) {
                                    obj.remove(param[3] + "_verified");

                                    Storeback.toFile(pfile, obj);

                                    channel.sendMessage(GetEmbed.green().setTitle("Removed verify").setDescription("The verify status of this profile has been removed"));
                                } else {
                                    channel.sendMessage(GetEmbed.red().setTitle("Not verified").setDescription("This specified profile is not verified, you can't remove the verify status of it"));
                                }
                            } else {
                                channel.sendMessage(GetEmbed.red().setTitle("Profile not found").setDescription("This profile doesn't exists for that user"));
                            }
                        } else {
                            channel.sendMessage(GetEmbed.red().setTitle("File not found").setDescription("The user profile storage file couldn't be found"));
                        }
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a profile"));
                    }
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a user"));
                }
            } else if (param[1].equalsIgnoreCase("profile")) {
                if (param.length > 2) {
                    if (param.length > 3) {
                        param[2] = param[2]
                                .replace("<", "")
                                .replace("@", "")
                                .replace("!", "")
                                .replace(">", "");
                        File pfile = new File(Get.dir + "/cache/users/" + param[2] + ".json");
                        if (pfile.exists()) {
                            JsonObject obj = GetObject.get(pfile);
                            if (Profile.exists(param[2], param[3])) {
                                if (Profile.isVerified(param[2], param[3])) {
                                    obj.remove(param[3] + "_verified");
                                }
                                obj.remove(param[3]);

                                Storeback.toFile(pfile, obj);

                                channel.sendMessage(GetEmbed.green().setTitle("Removed").setDescription("Profile (and the verify status of that profile) has been removed"));
                            } else {
                                channel.sendMessage(GetEmbed.red().setTitle("Profile not found").setDescription("The user hasn't set this profile"));
                            }
                        } else {
                            channel.sendMessage(GetEmbed.red().setTitle("Not found").setDescription("This file couldn't be found"));

                        }
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Not a valid profil").setDescription("This is not a valid profiltype"));
                    }
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a user"));
                }
            } else if (param[1].equalsIgnoreCase("user")) {
                if (param.length > 2) {
                    param[2] = param[2]
                            .replace("<", "")
                            .replace("@", "")
                            .replace("!", "")
                            .replace(">", "");
                    File pfile = new File(Get.dir + "/cache/users/" + param[2] + ".json");
                    if (ProfileFile.exists(param[2])) {
                        pfile.delete();
                        channel.sendMessage(GetEmbed.green().setTitle("Deleted!").setDescription("User storage file has been deleted"));
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Not found").setDescription("This file couldn't be found"));
                    }
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a user"));
                }
            } else if (param[1].equalsIgnoreCase("requests")) {
                File requestfile = new File(Get.dir + "/cache/requests.json");

                if (requestfile.exists()) {
                    requestfile.delete();
                    CreateFiles.requests();

                    channel.sendMessage(GetEmbed.green().setTitle("Reset").setDescription("All requests have been deleted and the file was reset"));
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Not found").setDescription("This file couldn't be found"));
                }
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Missing sub-command").setDescription("Please use one of the following sub-commands:\n- `profile <user> <profiletype> <name>`\n- `verify <user> <profiletype>`\n- `user <user>`\n- `requests`"));
        }
    }
}
