package io.github.servertoolsbot.profilebot.commands.all;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.logging.Exception;
import io.github.servertoolsbot.profilebot.util.profiles.ProfileFile;
import io.github.servertoolsbot.profilebot.util.profiles.Profilisize;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.user.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class RemoveProfile {
    public static void execute() {
        User user = Command.user;
        TextChannel channel = Command.channel;
        String[] param = Command.param;

        if (param.length > 1) {
            if (ProfileFile.exists(user)) {
                File pfile = new File(Get.dir + "/cache/users/" + user.getIdAsString() + ".json");

                try (FileReader reader = new FileReader(pfile)) {
                    if (GetKey.get(pfile, param[1]) != null || !reader.toString().equalsIgnoreCase("{}")) {
                        JsonObject obj = GetObject.get(pfile);

                        String oldProfileValue = obj.get(param[1]).getAsString();

                        String profiletype = Profilisize.execute(param);

                        obj.remove(param[1]);
                        obj.remove(param[1] + "_verified");

                        Storeback.toFile(pfile, obj);

                        BufferedReader br = new BufferedReader(reader);

                        String st;
                        StringBuilder sb = new StringBuilder();
                        while ((st = br.readLine()) != null)
                            sb.append(st);

                        br.close();

                        if (sb.toString().startsWith("{}")) {
                            pfile = pfile.getCanonicalFile();
                            pfile.delete();
                        }

                        channel.sendMessage(GetEmbed.green().setTitle("Profile removed").setDescription("Profile `${0}` for ${1} has been removed".replace(Get.Var(0), oldProfileValue).replace(Get.Var(1), profiletype)));
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Profile not found").setDescription("It odesn't looks, like you added that profile"));
                    }
                } catch (IOException e) {
                    Exception.print(e);
                }
            } else {
                channel.sendMessage(GetEmbed.red().setTitle("No profiles found").setDescription("It doesn't seems, like you have added any profiles"));
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("More arguments").setDescription("More arguments required, please input the name of the platform you want your profile un-linked"));
        }
    }
}
