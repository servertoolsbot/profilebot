package io.github.servertoolsbot.profilebot.commands.approver;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.Is;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.logging.Exception;
import io.github.servertoolsbot.profilebot.util.profiles.Profile;
import io.github.servertoolsbot.profilebot.util.profiles.Profilisize;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.user.User;

import java.io.File;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class VerifyList {
    public static void execute() {
        StringBuilder desc = new StringBuilder();
        File requestfile = new File(Get.dir + "cache/requests.json");
        JsonObject obj = GetObject.get(requestfile);
        Set<String> array = obj.keySet();
        DiscordApi api = ApiInstance.get();

        for (String s : array) {
            String[] list = GetKey.get(requestfile, s).getAsString().split("_");

            if (Is.Long(list[0])) {
                String username = "Unkown";
                User user = api.getYourself();
                try {
                    if (api.getUserById(list[0]).isDone()) {
                        user = api.getUserById(list[0]).get();
                        username = user.getDiscriminatedName();
                    }
                } catch (InterruptedException | ExecutionException e) {
                    Exception.print(e);
                }

                if (Profile.exists(user, list[1])) {
                    desc.append("\n- " + username + ": [" + Profilisize.execute(list, 1) + "](" + Profile.getLink(user.getIdAsString(), list[1]) + ") (ID: " + s + ")".replace(Get.Var(1), Profilisize.execute(list, 1).replace(Get.Var(2), Profile.getLink(user.getIdAsString(), list[1]))));
                } else {
                    obj.remove(s);
                    Storeback.toFile(requestfile, obj);
                    obj = GetObject.get(requestfile);
                }
            }
        }

        if (array.size() == 1) {
            desc.append("None");
        }

        Command.channel.sendMessage(GetEmbed.gray().setTitle("Current verify queue:").setDescription(desc.toString()));
    }
}
