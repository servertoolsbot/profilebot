package io.github.servertoolsbot.profilebot.commands.approver;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.profiles.Profile;
import io.github.servertoolsbot.profilebot.util.profiles.ProfileFile;
import org.javacord.api.entity.channel.TextChannel;

import java.io.File;

public class Add {
    public static void execute() {
        String[] param = Command.param;
        TextChannel channel = Command.channel;

        if (param.length > 1) {
            if (param[1].equalsIgnoreCase("verify")) {
                if (param.length > 2) {
                    if (param.length > 3) {
                        param[2] = param[2]
                                .replace("<", "")
                                .replace("@", "")
                                .replace("!", "")
                                .replace(">", "");
                        File pfile = new File(Get.dir + "/cache/users/" + param[2] + ".json");
                        if (ProfileFile.exists(param[2])) {
                            JsonObject obj = GetObject.get(pfile);
                            if (Profile.exists(param[2], param[3])) {
                                if (!Profile.isVerified(param[2], param[3])) {
                                    obj.addProperty(param[3] + "_verified", true);

                                    Storeback.toFile(pfile, obj);

                                    channel.sendMessage(GetEmbed.green().setTitle("Verify added").setDescription("The verify status of this profile has been added"));
                                } else {
                                    channel.sendMessage(GetEmbed.red().setTitle("Verified").setDescription("This specified profile is already verified"));
                                }
                            } else {
                                channel.sendMessage(GetEmbed.red().setTitle("Profile not found").setDescription("This profile doesn't exists for that user"));
                            }
                        } else {
                            channel.sendMessage(GetEmbed.red().setTitle("File not found").setDescription("The user profile storage file couldn't be found"));
                        }
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a profile"));
                    }
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a user"));
                }
            } else if (param[1].equalsIgnoreCase("profile")) {
                if (param.length > 2) {
                    if (param.length > 3) {
                        if (param.length > 4) {
                            param[2] = param[2]
                                    .replace("<", "")
                                    .replace("@", "")
                                    .replace("!", "")
                                    .replace(">", "");
                            File pfile = new File(Get.dir + "/cache/users/" + param[2] + ".json");
                            if (pfile.exists()) {
                                JsonObject obj = GetObject.get(pfile);
                                if (!Profile.exists(param[2], param[3])) {
                                    obj.addProperty(param[3], param[4]);

                                    Storeback.toFile(pfile, obj);

                                    channel.sendMessage(GetEmbed.green().setTitle("Added").setDescription("Profile has been added"));
                                } else {
                                    channel.sendMessage(GetEmbed.red().setTitle("Profile found").setDescription("The user has set this profile, please remove it first (using `${0}remove profile`)".replace(Get.Var(0), GetKey.settings("prefix"))));
                                }
                            } else {
                                channel.sendMessage(GetEmbed.red().setTitle("Not found").setDescription("This file couldn't be found"));

                            }
                        } else {
                            channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide the name of the profil"));
                        }
                    } else {
                        channel.sendMessage(GetEmbed.red().setTitle("Not a valid profil").setDescription("This is not a valid profiltype"));
                    }
                } else {
                    channel.sendMessage(GetEmbed.red().setTitle("Missing arguments").setDescription("Please provide a user"));
                }
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Missing sub-command").setDescription("Please use one of the following sub-commands:\n- `profile <user> <profiletype> <name>`\n- `verify <user> <profiletype>`"));
        }
    }
}
