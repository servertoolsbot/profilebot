package io.github.servertoolsbot.profilebot.commands.all;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.commands.CommandArrays;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.permissions.UserPermission;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.user.User;

public class Help {
    public static void execute() {
        TextChannel channel = Command.channel;
        User user = Command.user;
        String prefix = GetKey.settings("prefix");
        String[] allhelp = CommandArrays.allhelp;
        String[] alldesc = CommandArrays.alldesc;

        StringBuilder alldescembed = new StringBuilder();

        for (int i = 0; i < allhelp.length; i++) {
            alldescembed.append("\n- `${0}${1}` - ${2}".replace(Get.Var(0), prefix).replace(Get.Var(1), allhelp[i]).replace(Get.Var(2), alldesc[i]));
        }

        channel.sendMessage(GetEmbed.gray().setTitle("Commands").setDescription("${0}".replace(Get.Var(0), alldescembed.toString())));

        if (UserPermission.isAdmin(user, Command.event.getServer().get())) {
            String[] adminhelp = CommandArrays.adminhelp;
            String[] admindesc = CommandArrays.admindesc;

            StringBuilder admindescembed = new StringBuilder();

            for (int i = 0; i < adminhelp.length; i++) {
                alldescembed.append("\n- `${0}${1}` - ${2}".replace(Get.Var(0), prefix).replace(Get.Var(1), adminhelp[i]).replace(Get.Var(2), admindesc[i]));
            }

            channel.sendMessage(GetEmbed.gray().setTitle("Admin commands").setDescription("Admin permissions detected, you can use the following commands:\n\n*Currently there are no commands for admins*".replace(Get.Var(0), GetKey.settings("prefix"))));
        }

        if (UserPermission.isApprover(user)) {
            String[] approverhelp = CommandArrays.approverhelp;
            String[] approverdesc = CommandArrays.approverdesc;

            StringBuilder approverdescembed = new StringBuilder();

            for (int i = 0; i < approverhelp.length; i++) {
                approverdescembed.append("\n- `${0}${1}` - ${2}".replace(Get.Var(0), prefix).replace(Get.Var(1), approverhelp[i]).replace(Get.Var(2), approverdesc[i]));
            }

            channel.sendMessage(GetEmbed.gray().setTitle("Approver commands").setDescription("Approver access detected, you can use the following commands:\n\n${0}".replace(Get.Var(0), approverdescembed.toString())));
        }
    }
}
