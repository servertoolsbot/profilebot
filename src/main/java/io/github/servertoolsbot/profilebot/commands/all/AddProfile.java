package io.github.servertoolsbot.profilebot.commands.all;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.commands.CommandArrays;
import io.github.servertoolsbot.profilebot.util.CreateFiles;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.logging.Exception;
import io.github.servertoolsbot.profilebot.util.profiles.Profile;
import io.github.servertoolsbot.profilebot.util.profiles.ProfileFile;
import io.github.servertoolsbot.profilebot.util.profiles.Profilisize;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.user.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AddProfile {
    public static void execute() {
        TextChannel channel = Command.channel;
        User user = Command.user;
        String[] param = Command.param;

        if (param.length > 1) {
            String profiletype = Profilisize.execute(param, 1);

            if (param.length > 2) {
                String[] profiles = CommandArrays.profiles;
                for (int i = 0; i < profiles.length; i++) {
                    if (profiles[i].equalsIgnoreCase(param[1].toLowerCase())) {
                        File pfile = new File(Get.dir + "/cache/users/" + user.getIdAsString() + ".json");

                        JsonObject obj = null;
                        if (!ProfileFile.exists(user)) {
                            CreateFiles.cache();
                            try (FileWriter creator = new FileWriter(pfile)) {
                                creator.append("{}");
                                creator.flush();
                            } catch (IOException e) {
                                Exception.print(e);
                            }

                            obj = new JsonObject();
                        }

                        if (obj == null) {
                            obj = GetObject.get(pfile);
                        }

                        if (!Profile.exists(user, param[1])) {
                            obj.addProperty(param[1], param[2]);
                            Storeback.toFile(pfile, obj);

                            channel.sendMessage(GetEmbed.green().setTitle("Profile added").setDescription("Profile `${0}` for ${1} has been added".replace(Get.Var(0), param[2]).replace(Get.Var(1), profiletype)));
                        } else {
                            channel.sendMessage(GetEmbed.red().setTitle("Profile already added").setDescription("You have already added a profile for this profile, please remove it first (using `${0}removeprofile`)".replace(Get.Var(0), GetKey.settings("prefix"))));
                        }
                    } else {
                        if (i == profiles.length) {
                            channel.sendMessage(GetEmbed.red().setTitle("Can't add profile").setDescription("The profiletype ${0} is not allowed".replace(Get.Var(0), profiletype)));
                        }
                    }
                }
            } else {
                channel.sendMessage(GetEmbed.red().setTitle("More arguments").setDescription("Morge arguments required, please input the account name on your platform"));
            }
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("More arguments").setDescription("More arguments required, please input your desired profile type (list avaible profile types with `${0}types`)".replace(Get.Var(0), GetKey.settings("prefix"))));
        }
    }
}
