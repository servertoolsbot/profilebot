package io.github.servertoolsbot.profilebot.commands.all;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.commands.CommandArrays;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;

public class Types {
    public static void execute() {
        StringBuilder desc = new StringBuilder();

        String[] profiles = CommandArrays.profiles;
        String[] profiledis = CommandArrays.profiledis;
        String[] profilelinks = CommandArrays.rawlink;

        for (int i = 0; i < profiles.length; i++) {
            if (i != 0) {
                desc.append("\n- `${0}`: [${1}](${2})".replace(Get.Var(0), profiles[i]).replace(Get.Var(1), profiledis[i]).replace(Get.Var(2), profilelinks[i]));
            } else {
                desc.append("- `${0}`: [${1}](${2})".replace(Get.Var(0), profiles[i]).replace(Get.Var(1), profiledis[i]).replace(Get.Var(2), profilelinks[i]));
            }
        }

        Command.channel.sendMessage(GetEmbed.gray().setTitle("Avaible types").setDescription(desc.toString()));
    }
}
