package io.github.servertoolsbot.profilebot.commands.approver;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.logging.Log;

public class Stop {
    public static void execute() {
        Command.channel.sendMessage(GetEmbed.green().setTitle("Stopped").setDescription("The ApiInstance has been disconnected"));

        ApiInstance.get().disconnect();
        Log.info("Disconnected ApiInstance");
    }
}
