package io.github.servertoolsbot.profilebot.commands.approver;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.json.GetObject;
import io.github.servertoolsbot.profilebot.util.json.Storeback;
import io.github.servertoolsbot.profilebot.util.logging.Exception;
import io.github.servertoolsbot.profilebot.util.profiles.Profilisize;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.TextChannel;

import java.io.File;
import java.util.concurrent.ExecutionException;

public class VerifyConfirm {
    public static void execute() {
        String[] param = Command.param;
        TextChannel channel = Command.channel;
        File requestfile = new File(Get.dir + "/cache/requests.json");
        JsonObject obj = GetObject.get(requestfile);
        DiscordApi api = ApiInstance.get();

        if (param.length > 2) {
            String[] list = obj.get(param[2]).getAsString().split("_");
            File userfile = new File(Get.dir + "/cache/users/" + list[0] + ".json");
            JsonObject userobj = GetObject.get(userfile);
            userobj.addProperty(list[1] + "_verified", true);
            obj.remove(param[2]);

            Storeback.toFile(requestfile, obj);
            Storeback.toFile(userfile, userobj);

            String username = "a user";
            try {
                if (api.getUserById(list[0]).isDone()) {
                    username = ApiInstance.get().getUserById(list[0]).get().getName();
                }
            } catch (ExecutionException | InterruptedException e) {
                Exception.print(e);
            }
            channel.sendMessage(GetEmbed.green().setTitle("Verified!").setDescription("You verified the " + Profilisize.execute(list, 1) + " profil of " + username));
        } else {
            channel.sendMessage(GetEmbed.red().setTitle("Missing id").setDescription("Please provide the ID you want to confirm"));
        }
    }
}

