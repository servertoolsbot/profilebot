package io.github.servertoolsbot.profilebot.commands;

public class CommandArrays {
    public static final String[] all = new String[]{"help", "addprofile", "removeprofile", "listprofiles", "types", "verify", "info"};
    public static final String[] admin = new String[]{""};
    public static final String[] approver = new String[]{"stop", "setactivity", "say", "remove", "add"};

    public static final String[] allhelp = new String[]{"help", "addprofile <platform> <name>", "removeprofile <platform>", "listprofiles <user>", "types", "verify request <profiletype>", "info"};
    public static final String[] adminhelp = new String[]{""};
    public static final String[] approverhelp = new String[]{"stop", "setactivity [--type <activity>] <activity message>", "say <message>", "verify list", "verify confirm <id>", "remove <args>", "add <args>"};

    public static final String[] alldesc = new String[]{"Shows this help menu", "Lists your profiles", "Adds the profil for the platform to your profile list", "Removes the profile assigned to the platform", "Lists all avaible profiletypes", "Requests a verifiction", "Shows some informtion about the bot"};
    public static final String[] admindesc = new String[]{""};
    public static final String[] approverdesc = new String[]{"Stops the bot", "Sets the activity status of the bot", "The bot will say the provided message", "Shows the verification queue", "Confirms the verification request assigned with that id", "The bot will remove something", "The bot will add something"};


    public static final String[] profiles = new String[]{"github", "gitlab", "crowdin", "gamepedia", "fandom"};
    public static final String[] profiledis = new String[]{"GitHub", "GitLab", "Crowdin", "Gamepedia", "Fandom"};
    public static final String[] rawlink = new String[]{"https://github.com", "https://gitlab.com", "https://crowdin.com", "https://gamepedia.com", "https://fandom.com"};
    public static final String[] profilelinks = new String[]{"https://github.com/", "https://gitlab.com/", "https://crowdin.com/profile/", "https://help.gamepedia.com/UserProfile:", "https://community.fandom.com/wiki/User:"};
}
