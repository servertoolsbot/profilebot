package io.github.servertoolsbot.profilebot.event;

import io.github.servertoolsbot.profilebot.commands.Command;
import io.github.servertoolsbot.profilebot.commands.CommandArrays;
import io.github.servertoolsbot.profilebot.commands.all.*;
import io.github.servertoolsbot.profilebot.commands.approver.*;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.GetEmbed;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import io.github.servertoolsbot.profilebot.util.permissions.NoPermission;
import io.github.servertoolsbot.profilebot.util.permissions.UserPermission;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

public class MessageCreate {
    public static void call(MessageCreateEvent event) {
        if (!event.getMessageAuthor().isYourself()) {
            long mil = System.currentTimeMillis();
            if (event.getServer().isPresent()) {
                if (event.getServerTextChannel().isPresent()) {
                    if (event.getMessageAuthor().asUser().isPresent()) {
                        User user = event.getMessageAuthor().asUser().get();
                        if (!user.isBot()) {
                            String prefix = GetKey.settings("prefix");

                            if (event.getMessageContent().startsWith(prefix)) {
                                event(event, prefix);
                            }
                        }
                    }
                }
            }
            //event.getChannel().sendMessage("```" + event.getMessageContent() + "```");
            //event.getChannel().sendMessage(String.valueOf(System.currentTimeMillis() - mil));
        }
    }

    public static void event(MessageCreateEvent event, String prefix) {
        String[] param = event.getMessageContent().split(" ");
        String command = param[0].replace(prefix, "").toLowerCase();

        TextChannel channel = event.getChannel();
        User user = event.getMessageAuthor().asUser().get();

        MessageCreate.event.command = command;
        MessageCreate.event.channel = channel;
        MessageCreate.event.event = event;
        MessageCreate.event.user = user;
        MessageCreate.event.author = event.getMessageAuthor();
        MessageCreate.event.param = param;
        MessageCreate.event.content = event.getMessageContent().replace(prefix + command + " ", "").split(" ");

        Command.copyToClass(event, command, event.getChannel(), user, event.getMessageAuthor(), param, event.getMessageContent().replace(prefix + command + " ", "").split(" "));

        for (String s : CommandArrays.all) {
            if (s.equalsIgnoreCase(command)) {
                MessageCreate.event.all();
            }
        }

        for (String s : CommandArrays.admin) {
            if (s.equalsIgnoreCase(command)) {
                if (event.getMessageAuthor().isServerAdmin()) {
                    if (s.equalsIgnoreCase(command)) {
                        MessageCreate.event.admin();
                    }
                } else {
                    NoPermission.admin(channel);
                }
            }
        }

        for (String s : CommandArrays.approver) {
            if (s.equalsIgnoreCase(command)) {
                if (UserPermission.isApprover(event.getMessageAuthor().asUser().get())) {
                    MessageCreate.event.approver();
                } else {
                    NoPermission.approver(channel);
                }
            }
        }
    }

    public static class event {
        private static final DiscordApi api = ApiInstance.get();
        private static MessageCreateEvent event;
        private static String command;
        private static TextChannel channel;
        private static User user;
        private static MessageAuthor author;
        private static String[] param;
        private static String[] content;

        public static void all() {
            if (command.equalsIgnoreCase("listprofiles")) {
                ListProfiles.execute();
            } else if (command.equalsIgnoreCase("addprofile")) {
                AddProfile.execute();
            } else if (command.equalsIgnoreCase("removeprofile")) {
                RemoveProfile.execute();
            } else if (command.equalsIgnoreCase("help")) {
                Help.execute();
            } else if (command.equalsIgnoreCase("types")) {
                Types.execute();
            } else if (command.equalsIgnoreCase("verify")) {
                if (param.length > 1) {
                    if (param[1].equalsIgnoreCase("request")) {
                        VerifyRequest.execute();
                    } else if (param[1].equalsIgnoreCase("confirm")) {
                        if (UserPermission.isApprover(user)) {
                            VerifyConfirm.execute();
                        } else {
                            NoPermission.approver(channel);
                        }
                    } else if (param[1].equalsIgnoreCase("list")) {
                        if (UserPermission.isApprover(user)) {
                            VerifyList.execute();
                        } else {
                            NoPermission.approver(channel);
                        }
                    }
                } else {
                    String desc = "- `${0}verify request <profiletype>` - Sends a new request";

                    if (UserPermission.isApprover(user)) {
                        desc += "\n\n**As approver:**\n- `${0}verify confirm <id>` - Confirms a request";
                    }

                    desc = desc.replace(Get.Var(0), GetKey.settings("prefix"));

                    channel.sendMessage(GetEmbed.red().setTitle("Missing sub-commands").setDescription(desc));
                }
            } else if (command.equalsIgnoreCase("info")) {
                Info.execute();
            }
        }

        public static void admin() {
        }

        public static void approver() {
            if (command.equalsIgnoreCase("stop")) {
                Stop.execute();
            } else if (command.equalsIgnoreCase("setactivity")) {
                SetActivity.execute();
            } else if (command.equalsIgnoreCase("say")) {
                Say.execute();
            } else if (command.equalsIgnoreCase("remove")) {
                Remove.execute();
            } else if (command.equalsIgnoreCase("add")) {
                Add.execute();
            }
        }
    }
}
