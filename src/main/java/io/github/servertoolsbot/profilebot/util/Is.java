package io.github.servertoolsbot.profilebot.util;

import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;

public class Is {
    public static boolean Long(String aLong) {
        return aLong.length() == 18;
    }

    public static boolean UserMention(String mention) {
        if (mention.contains("<")) mention.replace("<", "");
        if (mention.contains("@")) mention.replace("@", "");
        if (mention.contains("!")) mention.replace("!", "");
        if (mention.contains(">")) mention.replace(">", "");
        return Long(mention) && ApiInstance.get().getChannelById(mention).isPresent();
    }

    public static boolean ChannelMention(String mention) {
        if (mention.contains("<")) mention.replace("<", "");
        if (mention.contains("#")) mention.replace("#", "");
        if (mention.contains(">")) mention.replace(">", "");
        return Long(mention) && ApiInstance.get().getUserById(mention).isDone();
    }
}
