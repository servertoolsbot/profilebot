package io.github.servertoolsbot.profilebot.util.profiles;

import io.github.servertoolsbot.profilebot.util.Get;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.user.User;

import java.io.File;

public class ProfileFile {
    public static boolean exists(String id) {
        return new File(Get.dir + "/cache/users/" + id + ".json").exists();
    }

    public static boolean exists(Long id) {
        return exists(String.valueOf(id));
    }

    public static boolean exists(User user) {
        return exists(user.getIdAsString());
    }

    public static boolean exists(MessageAuthor author) {
        return exists(author.getIdAsString());
    }
}
