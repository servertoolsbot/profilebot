package io.github.servertoolsbot.profilebot.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.server.Server;

import java.io.File;

public class Get {
    public static File settings() {
        File settings = new File(Get.dir + "/settings.json");
        if (settings.exists()) {
            return settings;
        } else {
            CreateFiles.settings();
        }

        return null;
    }

    public static Gson gson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }

    public static Server homeserver() {
        DiscordApi api = ApiInstance.get();
        if (api.getServerById(Long.valueOf(GetKey.settings("homeserverid"))).isPresent()) {
            return api.getServerById(Long.valueOf(GetKey.settings("homeserverid"))).get();
        }

        return null;
    }

    public static String Var(Integer i) {
        return "${" + i + "}";
    }

    public static String dir;
}
