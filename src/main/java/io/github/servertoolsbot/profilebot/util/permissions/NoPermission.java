package io.github.servertoolsbot.profilebot.util.permissions;

import io.github.servertoolsbot.profilebot.util.GetEmbed;
import org.javacord.api.entity.channel.TextChannel;

public class NoPermission {
    public static void admin(TextChannel channel) {
        channel.sendMessage(GetEmbed.red().setTitle("No permission").setDescription("You don't have admin rights on this server"));
    }

    public static void approver(TextChannel channel) {
        channel.sendMessage(GetEmbed.red().setTitle("No permission").setDescription("You don't have approver access"));
    }

}
