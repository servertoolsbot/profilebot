package io.github.servertoolsbot.profilebot.util.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.Start;
import io.github.servertoolsbot.profilebot.util.Format;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.logging.Log;

import java.io.File;

public class GetKey {
    public static String settings(String key) {
        File settings = Get.settings();
        if (Start.isExternal && (key.equalsIgnoreCase("homeserverid") || key.equalsIgnoreCase("token") || key.equalsIgnoreCase("approverid"))) {
            settings = new File(Get.dir + "/../servertoolsbot/settings.json");
            if (key.equalsIgnoreCase("approverid")) {
                key = "botadminid";
            }
        }
        return Format.toUnicode(io.github.servertoolsbot.profilebot.util.json.GetKey.get(settings, key).getAsString());
    }

    public static String settingsMaster(String key) {
        File settings = new File(Get.dir + "/../servertoolsbot/settings.json");
        return Format.toUnicode(get(settings, key).getAsString());
    }

    public static JsonElement get(File file, String key) {
        JsonObject obj = GetObject.get(file);
        Log.debug("Got JSON-Key \"" + key + "\"");
        try {
            if (!obj.get(key).isJsonNull()) {
                return obj.get(key);
            }
        } catch (NullPointerException e) {
            return null;
        }
        return null;
    }

    public static String asString(File file, String key) {
        return Format.toUnicode(io.github.servertoolsbot.profilebot.util.json.GetKey.get(file, key).getAsString());
    }

    public static Long asLong(File file, String key) {
        return io.github.servertoolsbot.profilebot.util.json.GetKey.get(file, key).getAsLong();
    }
}
