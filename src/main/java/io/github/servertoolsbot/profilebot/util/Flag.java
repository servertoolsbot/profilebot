package io.github.servertoolsbot.profilebot.util;

public class Flag {
    public static boolean hasFlag(String flagname, String[] input) {
        flagname = toFlag(flagname);

        for (int i = 0; i < input.length; i++) {
            if (input[i].equalsIgnoreCase(flagname)) {
                if (i + 1 <= input.length) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasFlag(String flagname, String input) {
        return hasFlag(flagname, new String[]{input});
    }

    public static String getFlag(String flagname, String[] input) {
        for (int i = 0; i < input.length; i++) {
            if (hasFlag(flagname, input[i])) {
                return input[i + 1];
            }
        }
        return "";
    }

    public static String toFlag(String flagname) {
        if (!flagname.startsWith("--")) {
            return "--" + flagname;
        }
        return flagname;
    }

    public static String withoutFlags(String flagname, String input) {
        return input.replace(" " + toFlag(flagname), "");
    }

    public static String withoutFlags(String flagname, String[] input) {
        return withoutFlags(flagname, input.toString());
    }
}
