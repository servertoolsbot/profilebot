package io.github.servertoolsbot.profilebot.util.json;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.util.CreateFiles;

import java.io.File;

public class Storeback {
    public static void toFile(File file, JsonObject obj) {
        file.delete();
        CreateFiles.generate(file, obj);
    }
}
