package io.github.servertoolsbot.profilebot.util.json;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.github.servertoolsbot.profilebot.util.CreateFiles;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.logging.Exception;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GetObject {
    public static JsonObject settings() {
        File settings = Get.settings();

        if (settings.exists()) {
            return io.github.servertoolsbot.profilebot.util.json.GetObject.get(settings);
        } else {
            CreateFiles.settings();
        }

        return null;
    }

    public static JsonObject get(File file) {
        if (file.exists()) {
            try (FileReader reader = new FileReader(file)) {
                JsonParser parser = new JsonParser();
                return parser.parse(reader).getAsJsonObject();
            } catch (IOException e) {
                Exception.print(e);
            }
        }

        return null;
    }
}
