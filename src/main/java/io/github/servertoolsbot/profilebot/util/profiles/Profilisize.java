package io.github.servertoolsbot.profilebot.util.profiles;

import io.github.servertoolsbot.profilebot.commands.CommandArrays;

public class Profilisize {
    public static String execute(String[] param, Integer position) {
        String profiletype = param[position];
        String[] profiles = CommandArrays.profiles;
        String[] profiledis = CommandArrays.profiledis;

        for (int i = 0; i < profiles.length; i++) {
            if (profiletype.equalsIgnoreCase(profiles[i])) {
                profiletype = profiledis[i];
            }
        }

        return profiletype;
    }

    public static String execute(String[] param) {
        return execute(param, 1);
    }
}
