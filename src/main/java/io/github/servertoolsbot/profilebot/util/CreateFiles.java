package io.github.servertoolsbot.profilebot.util;

import com.google.gson.JsonObject;
import io.github.servertoolsbot.profilebot.util.logging.Exception;
import io.github.servertoolsbot.profilebot.util.logging.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateFiles {
    public static void all() {
        CreateFiles.settings();

        CreateFiles.caching();
    }

    public static void caching() {
        CreateFiles.cache();
    }

    public static void settings() {
        Log.fatal("settings.json not found, creating...");

        JsonObject obj = new JsonObject();

        obj.addProperty("token", "-- YOUR TOKEN HERE --");
        obj.addProperty("prefix", "pr!");

        obj.addProperty("homeserverid", 564837852718956580L);
        obj.addProperty("approverid", 581118049936080906L);
        obj.addProperty("verifyrequestchannel", 596679889201922068L);

        CreateFiles.generate(new File(Get.dir + "/settings.json"), obj);
    }

    public static void cache() {
        File cache = new File(Get.dir + "/cache");
        cache.mkdir();
        requests();
        File users = new File(Get.dir + "/cache/users");
        users.mkdir();
    }

    public static void requests() {
        JsonObject obj = new JsonObject();
        obj.addProperty("latestid", 0);
        CreateFiles.generate(new File(Get.dir + "/cache/requests.json"), obj);
    }

    public static void generate(File file, JsonObject jsonObject) {
        try (FileWriter creator = new FileWriter(file)) {
            creator.append(Get.gson().toJson(jsonObject));

            creator.flush();

            Log.info("Created " + file.getCanonicalPath());
        } catch (IOException e) {
            Exception.print(e);
        }
    }
}
