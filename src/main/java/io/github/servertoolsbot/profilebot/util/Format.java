package io.github.servertoolsbot.profilebot.util;

public class Format {
    public static String toUnicode(String input) {
        return input
                .replace("ä", "\u00E4")
                .replace("ü", "\u00FC")
                .replace("ö", "\u00F6");
    }
}
