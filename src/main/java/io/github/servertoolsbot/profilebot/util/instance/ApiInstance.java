package io.github.servertoolsbot.profilebot.util.instance;

import io.github.servertoolsbot.profilebot.util.json.GetKey;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

public class ApiInstance {
    private static DiscordApi instance = null;

    public static DiscordApi get() {
        if (instance == null) {
            initalize();
        }

        return instance;
    }

    public static void initalize() {
        instance = new DiscordApiBuilder().setToken(GetKey.settings("token")).login().join();
    }
}
