package io.github.servertoolsbot.profilebot.util.profiles;

import io.github.servertoolsbot.profilebot.commands.CommandArrays;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.user.User;

import java.io.File;

public class Profile {
    public static boolean exists(String id, String profil) {
        return ProfileFile.exists(id) /**/&& GetKey.get(new File(Get.dir + "/cache/users/" + id + ".json"), profil) != null/**/;
    }

    public static boolean exists(User user, String profil) {
        return exists(user.getIdAsString(), profil);
    }

    public static boolean exists(MessageAuthor author, String profil) {
        return exists(author.getIdAsString(), profil);
    }

    public static boolean exists(Long id, String profil) {
        return exists(String.valueOf(id), profil);
    }


    public static boolean isVerified(String id, String profil) {
        return ProfileFile.exists(id) && GetKey.get(new File(Get.dir + "/cache/users/" + id + ".json"), profil + "_verified") != null;
    }

    public static boolean isVerified(User user, String profil) {
        return isVerified(user.getIdAsString(), profil);
    }

    public static boolean isVerified(MessageAuthor author, String profil) {
        return isVerified(author.getIdAsString(), profil);
    }

    public static boolean isVerified(Long id, String profil) {
        return isVerified(String.valueOf(id), profil);
    }


    public static boolean isProfileType(String profile) {
        Boolean bool = false;
        for (String s : CommandArrays.profiles) {
            if (s.equalsIgnoreCase(profile)) {
                if (!bool) {
                    bool = true;
                }
            }
        }

        return bool;
    }


    public static String getLink(String userid, String profil) {
        if (isProfileType(profil) && ProfileFile.exists(userid) && exists(userid, profil)) {
            String[] profiles = CommandArrays.profiles;
            for (int i = 0; i < profiles.length; i++) {
                if (profiles[i].equalsIgnoreCase(profil)) {
                    return CommandArrays.profilelinks[i] + GetKey.get(new File(Get.dir + "/cache/users/" + userid + ".json"), profil).getAsString();
                }
            }
        }

        return "";
    }
}
