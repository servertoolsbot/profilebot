package io.github.servertoolsbot.profilebot.util.permissions;

import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

public class UserPermission {
    private static boolean external = false;

    public static boolean isApprover(User user) {
        return user.getRoles(Get.homeserver()).toString().contains(GetKey.settings("approverid"));
    }

    public static boolean isAdmin(User user, Server server) {
        return server.isAdmin(user) || UserPermission.isApprover(user);
    }
}
