package io.github.servertoolsbot.profilebot.util.logging;

public class Log {
    public static void debug(String debug) {
        System.out.println("[LOG >> Debug] " + debug);
    }

    public static void info(String info) {
        System.out.println("[LOG >> Info] " + info);
    }

    public static void error(String error) {
        System.out.println("[LOG >> Error] " + error);
    }

    public static void fatal(String fatal) {
        System.out.println("[LOG >> Fatal] " + fatal);
    }
}
