package io.github.servertoolsbot.profilebot.util;

import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.awt.*;

public class GetEmbed {
    public static EmbedBuilder normal() {
        return new EmbedBuilder()
                .setAuthor(ApiInstance.get().getYourself())
                .setTimestampToNow();
    }

    public static EmbedBuilder colored(Color color) {
        return io.github.servertoolsbot.profilebot.util.GetEmbed.normal().setColor(color);
    }

    public static EmbedBuilder gray() {
        return io.github.servertoolsbot.profilebot.util.GetEmbed.colored(Color.GRAY);
    }

    public static EmbedBuilder red() {
        return io.github.servertoolsbot.profilebot.util.GetEmbed.colored(Color.RED);
    }

    public static EmbedBuilder green() {
        return io.github.servertoolsbot.profilebot.util.GetEmbed.colored(Color.GREEN);
    }
}
