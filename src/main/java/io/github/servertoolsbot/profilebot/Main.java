package io.github.servertoolsbot.profilebot;

import io.github.servertoolsbot.profilebot.event.MessageCreate;
import io.github.servertoolsbot.profilebot.util.json.GetKey;
import org.javacord.api.DiscordApi;

class Main {
    static void call(DiscordApi api) {
        api.updateActivity(GetKey.settings("prefix") + "help");

        api.addMessageCreateListener(MessageCreate::call);
    }
}
