package io.github.servertoolsbot.profilebot;

import io.github.servertoolsbot.profilebot.util.CreateFiles;
import io.github.servertoolsbot.profilebot.util.Get;
import io.github.servertoolsbot.profilebot.util.instance.ApiInstance;
import io.github.servertoolsbot.profilebot.util.logging.Log;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.permission.Permissions;

import java.io.File;

public class Start {
    public static boolean isExternal = false;

    public static void main(String[] args) {
        if (args.length < 1) {
            Get.dir = args[0];
            isExternal = true;
        }
        File settings = Get.settings();
        if (settings.exists()) {
            Start.login();
        } else {
            CreateFiles.all();
        }
    }

    private static void login() {
        ApiInstance.initalize();
        DiscordApi api = ApiInstance.get();

        Log.info("Logged in!");
        System.out.println(api.createBotInvite(Permissions.fromBitmask(0)));

        Main.call(api);
    }
}
